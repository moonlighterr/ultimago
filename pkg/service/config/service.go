package config

import "UltimaGo/pkg/service/config/config"

func LoadConfig(obj any, filename string, parseFn config.ParseFunction) {
	parseFn(filename, obj)
}
