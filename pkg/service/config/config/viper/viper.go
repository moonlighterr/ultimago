package viper

import (
	"UltimaGo/internal/service/logger"
	"errors"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"os"
)

func ParseFile(filename string, obj any) {
	viper.SetConfigName(filename) // name of config file (without extension)
	viper.SetConfigType("json")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")

	if _, err := os.Stat(filename + ".json"); errors.Is(err, os.ErrNotExist) {
		if _, err := os.Create(filename + ".json"); err != nil { // perm 0666
			logger.Warn("Creating config file continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
			return
		}
		if err = viper.WriteConfig(); err != nil {
			logger.Warn("Writing config file continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
			return
		}
	}

	err := viper.ReadInConfig()

	if err != nil {
		logger.Warn("Reading config continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
		return
	}

	viper.OnConfigChange(func(e fsnotify.Event) {
		err = viper.ReadInConfig()

		if err != nil {
			logger.Warn("Reloading config continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
			return
		}

		err = viper.Unmarshal(obj)
		if err != nil {
			logger.Warn("Reloading values from config continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
			return
		}

		logger.Info("Reloading config", "service.config.config.viper.viper")
	})

	go func() {
		viper.WatchConfig()
	}()

	err = viper.Unmarshal(obj)
	if err != nil {
		logger.Warn("Reading values from config continues with error", "service.config.config.viper.viper", map[string]any{"error": err})
		return
	}
	logger.Info("Config loaded", "service.config.config.viper.viper")
}
