package logger

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
)

type Interface interface {
	ApplyMongo(ctx context.Context, mc *mongo.Client)
}
