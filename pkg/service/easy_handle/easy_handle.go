package easy_handle

import (
	"context"
	"os"
	"sync"
)

type Handle struct {
	ctx context.Context
	wg  *sync.WaitGroup
	ch  chan os.Signal
}

func New(ctx context.Context, wg *sync.WaitGroup, doneChan chan os.Signal) Interface {
	return &Handle{ctx: ctx, wg: wg, ch: doneChan}
}

func (h Handle) GetContext() context.Context {
	return h.ctx
}

func (h Handle) GetWaitGroup() *sync.WaitGroup {
	return h.wg
}

func (h Handle) GetDoneChan() chan os.Signal {
	return h.ch
}
