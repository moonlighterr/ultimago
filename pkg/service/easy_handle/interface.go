package easy_handle

import (
	"context"
	"os"
	"sync"
)

type Interface interface {
	GetContext() context.Context
	GetWaitGroup() *sync.WaitGroup
	GetDoneChan() chan os.Signal
}
