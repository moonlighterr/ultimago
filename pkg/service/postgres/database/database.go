package database

import (
	"fmt"
	"github.com/jackc/pgx"
)

var _db *pgx.Conn

func init() {
	var err error
	_db, err = pgx.Connect(pgx.ConnConfig{
		Host:     "127.0.0.1",
		Port:     5432,
		Database: "uo",
		User:     "postgres",
		Password: "test123",
	})
	if err != nil {
		fmt.Printf("postgres connection error: %s\n", err)
		panic(0)
	}
}

func GetPostgre() *pgx.Conn {
	return _db
}
