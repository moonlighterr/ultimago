package database

import (
	"UltimaGo/internal/service/logger"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

var _client *mongo.Client

func GetMongo(ctx context.Context, doneChan chan os.Signal) *mongo.Client {
	var err error
	_client, err = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		logger.Warn("Cannot connect to mongoDB", "mongo", map[string]any{"error": err})
		doneChan <- os.Interrupt
	}
	return _client
}
