package handle_type

type HanleType uint8

const (
	NEVER HanleType = iota
	FOREVER
	GO_DOWN_ON_TIMEOUT
	GO_DOWN_ON_LOSE_ACTIVITY
	GO_DOWN_IMMEDIATELY
)
