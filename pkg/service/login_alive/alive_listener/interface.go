package alive_listener

import (
	"UltimaGo/pkg/service/login_alive/status"
	"context"
	"net/url"
	"os"
)

type Interface interface {
	Listen(ctx context.Context) chan status.Status
}

type HandlerInterface interface {
	HandleAliveListener(ctx context.Context, doneChan chan os.Signal, url url.URL)
}
