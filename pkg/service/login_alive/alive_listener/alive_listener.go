package alive_listener

import (
	"UltimaGo/pkg/service/login_alive/status"
	"context"
	"net/http"
	"net/url"
	"time"
)

type Listener struct {
	url url.URL
}

func New(url url.URL) Interface {
	return &Listener{url: url}
}

func (l Listener) Listen(ctx context.Context) chan status.Status {
	ch := make(chan status.Status)
	var st status.Status
	var shutdown bool
	go func() {
		for !shutdown {
			_, err := http.Get(l.url.String())
			if err != nil {
				st = status.DEAD
			} else {
				if st == status.DEAD {
					st = status.RISE
				} else {
					st = status.ALIVE
				}
			}

			ch <- st
			time.Sleep(time.Second * 1)
		}

		close(ch)
	}()
	go func() {
		for {
			select {
			case <-ctx.Done():
				shutdown = true
				return
			}
		}
	}()

	return ch
}
