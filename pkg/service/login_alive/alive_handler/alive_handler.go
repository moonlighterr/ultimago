package alive_handler

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/pkg/service/easy_handle"
	"UltimaGo/pkg/service/login_alive/alive_listener"
	"UltimaGo/pkg/service/login_alive/handle_type"
	"UltimaGo/pkg/service/login_alive/status"
	"context"
	"net/url"
	"os"
	"time"
)

type AliveHandler struct {
	hType   handle_type.HanleType
	timeout bool
	status  status.Status
}

func (a AliveHandler) Contexted(aListener alive_listener.Interface, ctx context.Context) chan status.Status {
	return aListener.Listen(ctx)
}

func (a *AliveHandler) Serve(easy easy_handle.Interface, url url.URL) {
	defer easy.GetWaitGroup().Done()
	defer func() {
		logger.Warn("Stopping ping login server", "service.login_alive.alive_handler.alive_handler")
	}()
	var cancelFn context.CancelFunc
	var st status.Status
	originalContext := easy.GetContext()
	ctx := originalContext

	aListener := alive_listener.New(url)
loop:
	for st = range a.Contexted(aListener, ctx) {
		a.status = st
		switch st {
		case status.DEAD:
			if a.hType == handle_type.GO_DOWN_IMMEDIATELY {
				time.AfterFunc(3*time.Second, func() {
					logger.Info("Got new status from alive server", "service.login_alive.alive_handler.alive_handler", map[string]any{"status": st})
					easy.GetDoneChan() <- os.Interrupt
				})
				return
			}
			if a.hType == handle_type.GO_DOWN_ON_LOSE_ACTIVITY {
				// some callback to use in connections handler
				// setting up to go down on last connection drop
			}
			if a.hType == handle_type.GO_DOWN_ON_TIMEOUT {
				if a.timeout {
					cancelFn()
					time.AfterFunc(3*time.Second, func() {
						logger.Info("Got new status from alive server", "service.login_alive.alive_handler.alive_handler", map[string]any{"status": st})
						easy.GetDoneChan() <- os.Interrupt
					})
					return
				}

				a.timeout = true

				ctx, cancelFn = context.WithTimeout(context.Background(), 5*time.Second)
				goto loop
			}
		case status.RISE:
			if a.timeout {
				cancelFn()
				ctx = originalContext
			}
			// callback for connections handler
		case status.ALIVE:
			if a.timeout {
				cancelFn()
				ctx = originalContext
			}
		}
	}
}

func (a AliveHandler) GetStatus() status.Status {
	return a.status
}

func New(easy easy_handle.Interface, url url.URL, hType handle_type.HanleType) Interface {
	easy.GetWaitGroup().Add(1)
	alive := AliveHandler{
		hType: hType,
	}
	go alive.Serve(easy, url)
	return &alive
}
