package alive_handler

import (
	"UltimaGo/pkg/service/easy_handle"
	"UltimaGo/pkg/service/login_alive/status"
	"net/url"
)

type Interface interface {
	Serve(easy easy_handle.Interface, url url.URL)
	GetStatus() status.Status
}
