package status

type Status int8

const (
	DEAD Status = iota
	RISE
	ALIVE
)
