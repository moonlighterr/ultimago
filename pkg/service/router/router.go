package router

import (
	"UltimaGo/packets/common/packet"
	"UltimaGo/world/player"
	"context"
)

type Interface interface {
	Route(ctx context.Context, pk packet.Interface, pl player.Interface)
}
