package listener

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/pkg/service/easy_handle"
	"net"
	"os"
	"strconv"
)

func listen(easy easy_handle.Interface, port int, connectionChan chan<- net.Conn) {
	defer easy.GetWaitGroup().Done()
	server, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		logger.Warn("Stopping listening new tcp connections", "service.net.listener.listener", map[string]any{"error": err, "port": port})
		easy.GetDoneChan() <- os.Interrupt
		return
	}

	logger.Info("Starting listening new tcp connections", "service.net.listener.listener", map[string]any{"port": port})

	errorChan := make(chan error)
	innerConnections := make(chan net.Conn)

	defer close(innerConnections)

	go func() {
		for {
			conn, err := server.Accept()
			if err != nil {
				errorChan <- err
			}

			logger.Info("Accepted connection", "service.net.listener.listener", map[string]any{"port": port, "connection": conn.RemoteAddr().String()})
			innerConnections <- conn
		}
	}()

	for {
		select {
		case <-easy.GetContext().Done():
			if err := server.Close(); err != nil {
				errorChan <- err
			} else {
				logger.Info("Shutdown", "service.net.listener.listener", map[string]any{"port": port})
			}
			return
		case innerConnectionsMessage := <-innerConnections:
			connectionChan <- innerConnectionsMessage
		case err := <-errorChan:
			close(errorChan)
			logger.Info("Shutdown", "service.net.listener.listener", map[string]any{"port": port, "error": err})
			easy.GetDoneChan() <- os.Interrupt
			return
		}
	}
}

func New(easy easy_handle.Interface, port int, connectionChan chan<- net.Conn) {
	easy.GetWaitGroup().Add(1)
	go listen(easy, port, connectionChan)
}
