package structs

import (
	"UltimaGo/packets/common/packet"
	"net"
)

type Packet struct {
	c net.Conn
	p packet.Interface
}

func (p Packet) Connection() net.Conn {
	return p.c
}

func (p Packet) Packet() packet.Interface {
	return p.p
}

func New(c net.Conn, p packet.Interface) *Packet {
	return &Packet{c, p}
}
