package connecter

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/reciever"
	"UltimaGo/pkg/service/easy_handle"
	"UltimaGo/pkg/struct/server_context"
	"UltimaGo/world/player"
	"UltimaGo/world/world"
	"context"
	"net"
)

func process(easy easy_handle.Interface, connectionsChan <-chan net.Conn, world world.Interface, packetFunction func(ctx context.Context, pk packet.Interface, pl player.Interface)) {
	defer easy.GetWaitGroup().Done()
	var connectionsContextCancelFunctions []context.CancelFunc
	for {
		select {
		case connection := <-connectionsChan:
			contextType := easy.GetContext().Value(server_context.KEY)
			connectionContext, connectionContextCancel := context.WithCancel(context.WithValue(context.Background(), server_context.KEY, contextType))
			playerInterface := player.New(connection, world, connectionContextCancel)
			playerInterface.FlushTimeout()
			packetsChan := make(chan packet.Interface)

			connectionsContextCancelFunctions = append(connectionsContextCancelFunctions, connectionContextCancel)
			go func() {
				if err := reciever.Recieve(connectionContext, connection, packetsChan); err != nil {
					logger.Warn("Connection close", "service.net.connecter.connecter", map[string]any{
						"error":      err.Error(),
						"connection": connection.RemoteAddr().String(),
					})
					connectionContextCancel()
				}
			}()

			go func() {
				for {
					select {
					case incomingPacket := <-packetsChan:
						go packetFunction(connectionContext, incomingPacket, playerInterface)
					case <-connectionContext.Done():
						close(packetsChan)
						connection.(*net.TCPConn).SetLinger(0)
						connection.Close()
						return
					}
				}
			}()
		case <-easy.GetContext().Done():
			for _, cf := range connectionsContextCancelFunctions {
				cf()
			}
			logger.Info("Shutdown", "service.net.connecter.connecter")
			return
		}
	}
}

func New(easy easy_handle.Interface, connectionsChan <-chan net.Conn, world world.Interface, packetFunction func(ctx context.Context, pk packet.Interface, pl player.Interface)) {
	easy.GetWaitGroup().Add(1)
	go process(easy, connectionsChan, world, packetFunction)
}
