package http

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/pkg/service/easy_handle"
	"net/http"
	"os"
	"strconv"
)

func ServeRoutes(easy easy_handle.Interface, port int, routes map[string]func(http.ResponseWriter, *http.Request)) {
	easy.GetWaitGroup().Add(1)
	mux := http.NewServeMux()
	for path, function := range routes {
		mux.HandleFunc(path, function)
	}
	server := http.Server{
		Addr:    ":" + strconv.Itoa(port),
		Handler: mux,
	}
	go func() {
		if err := server.ListenAndServe(); err != nil {
			easy.GetDoneChan() <- os.Interrupt
			logger.Warn("Error with listening to http connections", "service.net.http.http", map[string]any{
				"error": err,
				"port":  port,
			})
		}
	}()

	go func() {
		for {
			select {
			case <-easy.GetContext().Done():
				if err := server.Close(); err != nil {
					logger.Warn("Shutdown error", "service.net.http.http", map[string]any{
						"error": err,
						"port":  port,
					})
				} else {
					logger.Info("Shutdown", "service.net.http.http", map[string]any{
						"port": port,
					})
				}
				easy.GetWaitGroup().Done()
				return
			}
		}
	}()

	logger.Info("Starting listening http connections", "service.net.http.http", map[string]any{
		"port": port,
	})
}
