package character_list

import "UltimaGo/world/character"

type Interface interface {
	All() []character.Interface
	Count() int
	Load(accountId int32)
}
