package character_list

import (
	"UltimaGo/pkg/service/postgres/database"
	"UltimaGo/pkg/struct/parameter"
	"UltimaGo/pkg/struct/parameter_list"
	"UltimaGo/world/character"
	"fmt"
	"sync"
)

type characterList struct {
	data   []character.Interface
	mx     *sync.Mutex
	loaded bool
}

func (c characterList) All() []character.Interface {
	c.mx.Lock()
	defer c.mx.Unlock()
	charArr := make([]character.Interface, len(c.data))
	copy(charArr, c.data)

	return charArr
}

func (c *characterList) Load(accountId int32) {
	if !c.loaded {
		c.loaded = true
		rows, err := database.GetPostgre().Query(
			"SELECT id, name, account_id FROM characters WHERE account_id = $1",
			accountId,
		)
		if err != nil {
			fmt.Printf("error while loading chars: %s, account id %d\n", err, accountId)
		}

		for rows.Next() {
			params := parameter_list.New()

			var charId, charAccountId int
			var charName string

			if err = rows.Scan(&charId, &charName, &charAccountId); err != nil {
				fmt.Printf("error while scan chars: %s, account id %d\n", err, accountId)
			}

			params.Add(
				parameter.New[parameter.ID](charId),
				parameter.New[parameter.NAME](charName),
				parameter.New[parameter.ACCOUNT_ID](charAccountId),
			)

			char := character.New(params)
			c.data = append(c.data, char)
		}
	}
}

func (c characterList) Count() int {
	return len(c.data)
}

func New() Interface {
	return &characterList{
		data: []character.Interface{},
		mx:   &sync.Mutex{},
	}
}
