package parameter

type Interface interface {
	String() string
	Int8() int8
	Int16() int16
	Int32() int32
}
