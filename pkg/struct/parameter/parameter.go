package parameter

type Type interface {
	ID | SERIAL | NAME | PROFESSION | SEX |
		STR | DEX | INT |
		SKIN_COLOR | HAIR_COLOR | HAIR_STYLE | FACIAL_HAIR | FACIAL_HAIR_COLOR | SHIRT_COLOR | PANTS_COLOR |
		LOCATION | ACCOUNT_ID
}

type ID int32

type ACCOUNT_ID int32

type SERIAL int32
type NAME string
type PROFESSION int8
type SEX int8

type STR int8
type DEX int8
type INT int8

type SKIN_COLOR int16
type HAIR_STYLE int16
type HAIR_COLOR int16
type FACIAL_HAIR int16
type FACIAL_HAIR_COLOR int16
type SHIRT_COLOR int16
type PANTS_COLOR int16

type LOCATION int16

type Parameter[T Type] struct {
	value any
}

func (p Parameter[T]) String() string {
	return p.value.(string)
}

func (p Parameter[T]) Int8() int8 {
	return int8(p.value.(int))
}

func (p Parameter[T]) Int16() int16 {
	return int16(p.value.(int))
}

func (p Parameter[T]) Int32() int32 {
	switch p.value.(type) {
	case int:
		return int32(p.value.(int))
	case int8:
		return int32(p.value.(int8))
	case int16:
		return int32(p.value.(int16))
	case int32:
		return p.value.(int32)
	case int64:
		return int32(p.value.(int64))
	}
	return int32(p.value.(int))
}

func New[T Type](v any) Interface {
	return &Parameter[T]{value: v}
}
