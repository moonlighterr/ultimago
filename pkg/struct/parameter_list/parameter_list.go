package parameter_list

import (
	"UltimaGo/pkg/struct/parameter"
	"reflect"
	"sync"
)

type parameterList struct {
	data []parameter.Interface

	mx *sync.RWMutex
}

func (p *parameterList) Add(params ...parameter.Interface) {
	p.mx.Lock()
	defer p.mx.Unlock()

	for _, paramToAdd := range params {
		r1 := reflect.TypeOf(paramToAdd).Elem()
		for i, paramInList := range p.data {
			r2 := reflect.TypeOf(paramInList).Elem()
			if r2 == r1 {
				p.data[i] = paramToAdd
			}
		}

		p.data = append(p.data, paramToAdd)
	}
}

func (p parameterList) Each(fn func(param parameter.Interface) bool) []parameter.Interface {
	p.mx.RLock()
	defer p.mx.RUnlock()

	var params []parameter.Interface

	for _, parameterInList := range p.data {
		if fn(parameterInList) {
			params = append(params, parameterInList)
		}
	}
	return params
}

func (p parameterList) Find(fn func(param parameter.Interface) bool) parameter.Interface {
	res := p.Each(fn)
	if len(res) > 0 {
		return res[0]
	}
	return nil
}

func New() Interface {
	return &parameterList{
		data: []parameter.Interface{},
		mx:   &sync.RWMutex{},
	}
}

func Get[T parameter.Type](parameterList Interface) parameter.Interface {
	param := parameterList.Find(func(param parameter.Interface) bool {
		if _, ok := param.(*parameter.Parameter[T]); ok {
			return true
		}
		return false
	})
	return param
}
