package parameter_list

import "UltimaGo/pkg/struct/parameter"

type Interface interface {
	Add(params ...parameter.Interface)
	Each(fn func(param parameter.Interface) bool) []parameter.Interface
	Find(fn func(param parameter.Interface) bool) parameter.Interface
}
