package connection

import (
	"net"
	"sync"
)

type Pool struct {
	data []*Structure
	mx   *sync.RWMutex
}

func NewPool() *Pool {
	return &Pool{
		data: []*Structure{},
		mx:   &sync.RWMutex{},
	}
}

func (p *Pool) Add(connection *Structure) {
	p.mx.Lock()
	defer p.mx.Unlock()
	p.data = append(p.data, connection)
}

func (p *Pool) FindByConnection(c net.Conn) *Structure {
	p.mx.RLock()
	defer p.mx.RUnlock()
	for _, structure := range p.data {
		if structure.Connection == c {
			return structure
		}
	}
	return nil
}

func (p *Pool) FindBySeed(seed [4]byte) *Structure {
	p.mx.RLock()
	defer p.mx.RUnlock()
	for _, structure := range p.data {
		if structure.Seed == seed {
			return structure
		}
	}
	return nil
}

type Structure struct {
	Seed       [4]byte
	Connection net.Conn
	AccountId  int32
}
