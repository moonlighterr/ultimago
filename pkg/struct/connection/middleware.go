package connection

import (
	"context"
	"net"
)

type MiddlewareChain []Middleware

type Middleware func(ctx context.Context) net.Conn

func (m *MiddlewareChain) Add(mil ...Middleware) MiddlewareChain {
	for _, mi := range mil {
		*m = append(*m, mi)
	}
	return *m
}
