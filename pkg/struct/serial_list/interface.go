package serial_list

import "UltimaGo/pkg/struct/serial"

type Interface interface {
	Add(serial serial.Interface, obj any)
	Get(serial serial.Interface) any
}
