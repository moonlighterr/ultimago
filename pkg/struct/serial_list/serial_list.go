package serial_list

import (
	"UltimaGo/pkg/struct/serial"
	"sync"
)

type serialList struct {
	data map[serial.Interface]any
	mx   *sync.RWMutex
}

func (s *serialList) Add(serial serial.Interface, obj any) {
	s.mx.Lock()
	defer s.mx.Unlock()

	if _, ok := s.data[serial]; !ok {
		s.data[serial] = obj
	}
}

func (s *serialList) Get(serial serial.Interface) any {
	s.mx.RLock()
	defer s.mx.RUnlock()

	if obj, ok := s.data[serial]; ok {
		return obj
	}
	return nil
}

func (s serialList) FreeSerial() serial.Interface {
	s.mx.RLock()
	defer s.mx.RUnlock()
	lastIndex := int32(1)
	for si, _ := range s.data {
		if si.Get() == lastIndex {
			continue
		}
		if si.Get() > lastIndex+1 {
			return serial.New(lastIndex + 1)
		}
		lastIndex++
	}
	return serial.New(1)
}

func New() Interface {
	return &serialList{
		data: make(map[serial.Interface]any),
		mx:   &sync.RWMutex{},
	}
}
