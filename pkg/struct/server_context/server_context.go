package server_context

import "context"

const KEY string = "server_context"

type Type int

const (
	LOGIN Type = iota
	GAME
)

func GetContextServerType(t int) string {
	switch Type(t) {
	case LOGIN:
		return "login"
	case GAME:
		return "game"
	}
	return "simple"
}

func GetServerTypeContext(ctx context.Context, typ Type) context.Context {
	return context.WithValue(ctx, KEY, int(typ))
}
