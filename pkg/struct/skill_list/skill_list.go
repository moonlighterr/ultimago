package skill_list

import (
	"UltimaGo/world/skill"
	"sync"
)

type skillList struct {
	data []skill.Interface
	mx   *sync.Mutex
}

func (s *skillList) Add(skill skill.Interface) {
	s.mx.Lock()
	defer s.mx.Unlock()
	for _, skillInList := range s.data {
		if skillInList.GetID() == skill.GetID() {
			return
		}
	}

	s.data = append(s.data, skill)
}
