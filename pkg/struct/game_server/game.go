package game_server

import (
	"net"
	"time"
)

type server struct {
	serverName string
	address    net.Addr
}

func (s server) GetName() string {
	return s.serverName
}

func (s server) GetAddr() net.Addr {
	return s.address
}

func (s server) GetIP() net.IP {
	switch addr := s.address.(type) {
	case *net.TCPAddr:
		return addr.IP.To4()
	case *net.UDPAddr:
		return addr.IP.To4()
	}
	return nil
}

func (s server) GetPort() int {
	switch addr := s.address.(type) {
	case *net.TCPAddr:
		return addr.Port
	case *net.UDPAddr:
		return addr.Port
	}
	return 0
}

func (s server) GetTimeZone() time.Location {
	return time.Location{}
}

func (s server) GetFullPercentage() int {
	return 0
}

func New(serverName string, address net.Addr) Interface {
	return &server{
		serverName: serverName,
		address:    address,
	}
}
