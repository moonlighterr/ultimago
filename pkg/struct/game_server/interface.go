package game_server

import (
	"net"
	"time"
)

type Interface interface {
	GetName() string
	GetAddr() net.Addr
	GetIP() net.IP
	GetPort() int
	GetTimeZone() time.Location
	GetFullPercentage() int
}
