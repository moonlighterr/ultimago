package serial

type serial int32

func (s serial) Get() int32 {
	return int32(s)
}

func New(v int32) Interface {
	serial := serial(v)
	return &serial
}
