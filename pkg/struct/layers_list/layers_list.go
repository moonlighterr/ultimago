package layers_list

import (
	"UltimaGo/world/layer"
	"sync"
)

type layersList struct {
	data []layer.Interface
	mx   *sync.Mutex
}

func (l layersList) All() []layer.Interface {
	return l.data
}
