package layers_list

import "UltimaGo/world/layer"

type Interface interface {
	All() []layer.Interface
}
