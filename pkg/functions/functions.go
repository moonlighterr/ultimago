package functions

func ReverseBytes(o []byte) []byte {
	rO := make([]byte, len(o))
	for i, s := len(o), 0; i > 0; i, s = i-1, s+1 {
		rO[s] = o[i-1]
	}
	return rO
}
