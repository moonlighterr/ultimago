package player

import (
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/character_list"
	"UltimaGo/world/character"
	"UltimaGo/world/world"
	"context"
	"github.com/google/uuid"
	"net"
	"sync"
	"time"
)

type timeouts struct {
	timeoutmx   *sync.Mutex
	timeout     time.Time
	timeoutSync *sync.Once
}

type player struct {
	c             net.Conn
	lastPacket    packet.Interface
	uuid          uuid.UUID
	characters    character_list.Interface
	world         world.Interface
	accountId     int32
	timeouts      timeouts
	ctxCancelFunc context.CancelFunc

	char character.Interface
}

func (p *player) GetUUID() uuid.UUID {
	if p.uuid == uuid.Nil {
		p.uuid, _ = uuid.NewRandom()
	}

	return p.uuid
}

func (p *player) SetLastPacket(packet packet.Interface) {
	p.lastPacket = packet
}

func (p player) GetLastPacket() packet.Interface {
	return p.lastPacket
}

func (p player) GetConnection() net.Conn {
	return p.c
}

func (p player) CloseConnection() {
	p.c.Close()
}

func (p player) GetCharacterList() character_list.Interface {
	p.characters.Load(p.GetAccountId())
	return p.characters
}

func (p player) GetWorld() world.Interface {
	return p.world
}

func (p *player) SetAccountId(id int32) {
	p.accountId = id
}

func (p player) GetAccountId() int32 {
	return p.accountId
}

func (p player) Disconnect() {
	p.ctxCancelFunc()
}

func (p *player) SetCharacter(char character.Interface) {
	p.char = char
}

func (p player) GetCharacter() character.Interface {
	return p.char
}

func (p *player) FlushTimeout() {
	p.timeouts.timeoutmx.Lock()
	p.timeouts.timeout = time.Now()
	p.timeouts.timeoutSync.Do(func() {
		go func() {
			ticker := time.NewTicker(5 * time.Second)
			for _ = range ticker.C {
				p.timeouts.timeoutmx.Lock()
				if p.timeouts.timeout.Add(time.Second * 30).Before(time.Now()) {
					p.timeouts.timeoutmx.Unlock()
					p.Disconnect()
					return
				}
				p.timeouts.timeoutmx.Unlock()
			}
		}()
	})
	p.timeouts.timeoutmx.Unlock()
}

func New(c net.Conn, world world.Interface, ctxCancelFunc context.CancelFunc) Interface {
	return &player{
		c:          c,
		characters: character_list.New(),
		world:      world,
		timeouts: timeouts{
			timeoutmx:   &sync.Mutex{},
			timeout:     time.Now(),
			timeoutSync: &sync.Once{},
		},
		ctxCancelFunc: ctxCancelFunc,
	}
}
