package player

import (
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/character_list"
	"UltimaGo/world/character"
	"UltimaGo/world/world"
	"github.com/google/uuid"
	"net"
)

type Interface interface {
	GetConnection() net.Conn
	CloseConnection()
	GetUUID() uuid.UUID

	SetLastPacket(packet packet.Interface)
	GetLastPacket() packet.Interface

	GetCharacterList() character_list.Interface
	GetWorld() world.Interface

	SetAccountId(id int32)
	GetAccountId() int32

	Disconnect()

	SetCharacter(char character.Interface)
	GetCharacter() character.Interface

	FlushTimeout()
}
