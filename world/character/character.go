package character

import (
	"UltimaGo/pkg/service/postgres/database"
	"UltimaGo/pkg/struct/parameter"
	"UltimaGo/pkg/struct/parameter_list"
	"fmt"
)

type character struct {
	parameters parameter_list.Interface
}

func (c character) Save() {
	var id int32
	rawId := parameter_list.Get[parameter.ID](c.parameters)
	if rawId != nil {
		id = parameter_list.Get[parameter.ID](c.parameters).Int32()
	}

	if id == 0 {
		err := database.GetPostgre().QueryRow(
			"INSERT INTO characters(name, account_id) VALUES($1, $2) RETURNING id",
			parameter_list.Get[parameter.NAME](c.parameters).String(),
			parameter_list.Get[parameter.ACCOUNT_ID](c.parameters).Int32(),
		).Scan(&id)

		c.parameters.Add(parameter.New[parameter.ID](id))
		if err != nil {
			fmt.Printf("error while creating char %s\n", err)
		}
	} else {
		err := database.GetPostgre().QueryRow(
			"UPDATE characters SET name = $1, account_id = $2 WHERE id = $3",
			parameter_list.Get[parameter.NAME](c.parameters).String(),
			parameter_list.Get[parameter.ACCOUNT_ID](c.parameters).Int32(),
			id,
		)
		if err != nil {
			fmt.Printf("error while saving char %d %s\n", id, err)
		}
	}

}

func (c character) GetParameters() parameter_list.Interface {
	return c.parameters
}

func New(params parameter_list.Interface) Interface {
	return &character{
		parameters: params,
	}
}
