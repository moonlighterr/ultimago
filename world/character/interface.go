package character

import (
	"UltimaGo/pkg/struct/parameter_list"
	"UltimaGo/pkg/struct/save"
)

type Interface interface {
	save.Interface
	GetParameters() parameter_list.Interface
}
