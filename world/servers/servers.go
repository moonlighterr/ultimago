package servers

import (
	"UltimaGo/pkg/struct/game_server"
	"sync"
)

type serverList struct {
	list []game_server.Interface
	mx   *sync.Mutex
}

var servers = serverList{
	list: []game_server.Interface{},
	mx:   &sync.Mutex{},
}

func Add(s game_server.Interface) {
	if Exists(s) {
		return
	}

	servers.mx.Lock()
	defer servers.mx.Unlock()
	servers.list = append(servers.list, s)
}

func All() []game_server.Interface {
	returnServerList := make([]game_server.Interface, len(servers.list))
	servers.mx.Lock()
	defer servers.mx.Unlock()
	copy(returnServerList, servers.list)
	return returnServerList
}

func Index(i int) game_server.Interface {
	servers.mx.Lock()
	defer servers.mx.Unlock()
	if i > len(servers.list)-1 {
		return nil
	}
	return servers.list[i]
}

func Exists(s game_server.Interface) bool {
	servers.mx.Lock()
	defer servers.mx.Unlock()
	for _, serverInList := range servers.list {
		if serverInList.GetAddr().String() == s.GetAddr().String() {
			return true
		}
	}
	return false
}
