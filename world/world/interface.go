package world

import (
	"UltimaGo/pkg/struct/layers_list"
	"UltimaGo/pkg/struct/serial_list"
)

type Interface interface {
	GetLayers() layers_list.Interface
	GetSerials() serial_list.Interface
	Load() error
}
