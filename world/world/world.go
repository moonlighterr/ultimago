package world

import (
	"UltimaGo/pkg/struct/layers_list"
	"UltimaGo/pkg/struct/serial_list"
)

type world struct {
	layersList layers_list.Interface
	serialList serial_list.Interface
}

func (w world) GetLayers() layers_list.Interface {
	return w.layersList
}

func (w world) GetSerials() serial_list.Interface {
	return w.serialList
}

func (w world) Load() error {

	return nil
}

func New() Interface {
	return &world{
		serialList: serial_list.New(),
	}
}
