package common

type CommandOffset byte

const (
	OffsetSeed         CommandOffset = 0xEF
	OffsetLogin        CommandOffset = 0x80
	OffsetSelectServer CommandOffset = 0xA0

	OffsetGameServerLogin CommandOffset = 0x91

	OffsetPing              CommandOffset = 0x73
	OffsetCharacterCreation CommandOffset = 0xF8

	OffsetCharacterSelect CommandOffset = 0x5D
)

var command2type = map[byte]PacketType{
	byte(OffsetSeed):              TypeSeed,
	byte(OffsetLogin):             TypeLogin,
	byte(OffsetSelectServer):      TypeSelectServer,
	byte(OffsetGameServerLogin):   TypeGameServerLogin,
	byte(OffsetPing):              TypePing,
	byte(OffsetCharacterCreation): TypeCharacterCreation,
	byte(OffsetCharacterSelect):   TypeCharacterSelection,
}

func (c CommandOffset) PacketType() PacketType {
	return command2type[byte(c)]
}
