package field

func padBytes(init []byte, size int) []byte {
	if len(init) == size {
		return init
	}

	toAdd := size - len(init)
	boo := make([]byte, toAdd)
	for i := 0; i < toAdd; i++ {
		boo[i] = 0x00
	}
	return boo
}
