package field

import (
	"bytes"
	"encoding/binary"
)

// Interface of packet field
type Interface interface {
	Bytes

	GetName() Name
	GetSubFields() []Interface

	SetData(data interface{})

	GetSize() int
	Mutate(r []byte, offset int) (int, error)

	DumpString() string
}

// Bytes support interface, providing representing field as bytes
type Bytes interface {
	Bytes() []byte
}

// Type generic interface with all fields types
type Type interface {
	String | Int | Slice | PacketSize
}

type String string

func (f String) Get() string {
	return string(f)
}

func (f String) Bytes() []byte {
	return []byte(f)
}

type Int int

func (f Int) Get() int {
	return int(f)
}

func (f Int) Bytes() []byte {
	buf := bytes.Buffer{}
	_ = binary.Write(&buf, binary.BigEndian, f)
	return buf.Bytes()
}

type PacketSize Int

func (f PacketSize) Get() int {
	return int(f)
}

func (f PacketSize) Bytes() []byte {
	buf := bytes.Buffer{}
	_ = binary.Write(&buf, binary.BigEndian, f)
	return buf.Bytes()
}

type Slice []any

func (f Slice) Get() []any {
	return f
}

func (f Slice) Bytes() []byte {
	buf := bytes.Buffer{}
	for _, elem := range f {
		switch elem.(type) {
		case Field[Int], Field[PacketSize]:
			buf.Write(elem.(Field[Int]).Bytes())
		case Field[String]:
			buf.Write(elem.(Field[String]).Bytes())
		}
	}
	return buf.Bytes()
}
