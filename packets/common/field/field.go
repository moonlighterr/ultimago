package field

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"reflect"
	"strings"
)

type Name string

const Command Name = "command"
const Size Name = "size"

type Field[T Type] struct {
	name           Name
	size           int
	data           []byte
	subFields      []Interface
	_computedField Interface
}

func (f Field[T]) GetSize() int {
	return f.size
}

func (f Field[T]) GetName() Name {
	return f.name
}

func Get[T Type](field Interface) T {
	onType, _ := field.(*Field[T])
	fieldData := onType.GetData()

	return fieldData
}

func (f Field[T]) GetSubFields() []Interface {
	return f.subFields
}

func (f Field[T]) GetData() T {
	switch (interface{})(f).(type) {
	case Field[Int], Field[PacketSize]:
		var ints int
		switch len(f.data) {
		case 1:
			ints = int(f.data[0])
		case 2:
			ints = int(binary.BigEndian.Uint16(f.data))
		case 4:
			ints = int(binary.BigEndian.Uint32(f.data))
		case 8:
			ints = int(binary.BigEndian.Uint64(f.data))
		}
		return (interface{})(Int(ints)).(T)
	case Field[String]:
		str := strings.Trim(string(f.data), "\x00")
		return (interface{})(String(str)).(T)
	case Field[Slice]:
		return (interface{})(f.data).(T)
	default:
		z := reflect.TypeOf(f).Kind()
		panic(fmt.Sprintf("ERR ! field %s is %T\n", f.GetName(), z.String()))
	}
}

func (f *Field[T]) SetData(data interface{}) {
	buffer := bytes.Buffer{}
	if f.data != nil {
		buffer.Write(f.data)
	}

	switch res := data.(type) {
	case int:
		panic("non-range integer data cannot be set due to size")
	case int8, int16, int32, int64, uint8, uint16, uint32, uint64:
		intBuffer := bytes.Buffer{}
		err := binary.Write(&intBuffer, binary.BigEndian, res)
		if err != nil {
			fmt.Println(err)
		}
		buffer.Write(intBuffer.Bytes())
	case []byte:
		buffer.Write(res)
	case string:
		buffer.WriteString(res)
		if len(res) != f.GetSize() {
			buffer.Write(padBytes([]byte(res), f.GetSize()))
		}
	}

	f.data = buffer.Bytes()
}

func (f Field[T]) Bytes() []byte {
	return f.data
}

func (f *Field[T]) Mutate(r []byte, offset int) (int, error) {
	f.data = r[offset : offset+f.size]
	if len(f.data) != f.size {
		return 0, errors.New(
			fmt.Sprintf(
				"Field `%s` cannot successfully mutate buffer due to size difference, expect %d, actual %d",
				f.GetName(),
				f.size,
				len(f.data),
			),
		)
	}
	return f.size, nil
}

func (f Field[T]) DumpString() string {
	switch (interface{})(f).(type) {
	case Field[PacketSize]:
		return fmt.Sprintf(
			"field [len]: %-30s data: % X",
			f.GetName(),
			f.Bytes(),
		)
	case Field[Int]:
		return fmt.Sprintf(
			"field [int]: %-30s data: % X",
			f.GetName(),
			f.Bytes(),
		)
	case Field[String]:
		return fmt.Sprintf(
			"field [str]: %-30s data: % X",
			f.GetName(),
			f.Bytes(),
		)
	case Field[Slice]:
		fieldsString := make([]string, 0)
		fieldsString = append(fieldsString, fmt.Sprintf(
			"field [arr]: %-30s data: % X",
			f.GetName(),
			f.Bytes(),
		))
		for _, subField := range f.subFields {
			fieldsString = append(fieldsString, fmt.Sprintf("\t [subfield] > %s", subField.DumpString()))
		}
		return strings.Join(fieldsString, "\n")
	default:
		z := reflect.TypeOf(f).Kind()
		return fmt.Sprintf("ERR ! field %s is %T\n", f.GetName(), z.String())
	}
}

func New[T Type](name Name, size int, subField ...Interface) Interface {
	newField := &Field[T]{
		name:      name,
		size:      size,
		data:      nil,
		subFields: subField,
	}

	return (interface{})(newField).(Interface)
}
