package packet

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"bytes"
	"net"
)

type Packet struct {
	fields       Fields
	_type        common.PacketType
	_sized       bool
	needCompress bool
}

func (p Packet) GetCommandField() field.Interface {
	return GetField(&p, field.Command)
}

func (p *Packet) SetCommandField(f field.Interface) {
	var replaced bool
	for i, _field := range p.fields {
		if _field.(field.Interface).GetName() == field.Command {
			p.fields[i] = f
			replaced = true
		}
	}
	if !replaced && len(p.fields) == 0 {
		p.fields = append(p.fields, f)
	}
}

func (p Packet) GetType() common.PacketType {
	return p._type
}

func (p Packet) GetFields() Fields {
	return p.fields
}

func (p Packet) GetField(fieldName field.Name) field.Interface {
	fields := p.GetFields()
	for _, f := range fields {
		if f.GetName() == fieldName {
			return f
		}
	}
	return nil
}

func GetField(packet Interface, name field.Name) field.Interface {
	for _, f := range packet.GetFields() {
		rField := f.(field.Interface)
		if rField.GetName() != name {
			continue
		}
		return rField
	}

	return nil
}

func (p Packet) GetSize() int {
	var totalFieldsSize int
	for _, attr := range p.fields {
		totalFieldsSize += attr.(field.Interface).GetSize()
	}
	return totalFieldsSize
}

func (p *Packet) Mutate(r []byte, offset int) (int, error) {
	var err error
	if p.fields == nil {
		return 0, nil
	}
	var bytesReaded int
	for _, fieldInFields := range p.fields {
		bytesReaded, err = fieldInFields.(field.Interface).Mutate(r, offset)
		offset += bytesReaded
	}
	return offset, err
}
func (p Packet) Debug(c net.Conn) {
	var data string
	for _, fieldInFields := range p.GetFields() {
		data = string(fieldInFields.(field.Interface).GetName()) + " => " + fieldInFields.(field.Interface).DumpString() + "\n"
	}
	logger.Info("Packet structure", "packets.common.packet.packet", map[string]any{
		"packet_type": p.GetType(),
		"data":        data,
		"connection":  c.RemoteAddr().String(),
	})
}

func (p Packet) Bytes() []byte {
	capacityFieldValue := 0
	var capacityField *field.Field[field.PacketSize]

	buffer := bytes.Buffer{}
	for _, fieldInFields := range p.GetFields() {
		capacityFieldValue += fieldInFields.GetSize()
		if f, ok := fieldInFields.(*field.Field[field.PacketSize]); ok {
			capacityField = f
		}
		if subFields := fieldInFields.GetSubFields(); len(subFields) > 0 {
			for _, subField := range subFields {
				capacityFieldValue += subField.GetSize()
			}
		}
	}
	if capacityField != nil {
		l := capacityField.GetSize()
		switch {
		case l == 1:
			(*capacityField).SetData(int8(capacityFieldValue))
		case l == 2:
			(*capacityField).SetData(int16(capacityFieldValue))
		case l == 4:
			(*capacityField).SetData(int32(capacityFieldValue))
		case l == 8:
			(*capacityField).SetData(int64(capacityFieldValue))
		}
	}

	for _, fieldInFields := range p.GetFields() {
		buffer.Write(fieldInFields.(field.Interface).Bytes())
		if subFields := fieldInFields.GetSubFields(); len(subFields) > 0 {
			for _, subField := range subFields {
				buffer.Write(subField.(field.Interface).Bytes())
			}
		}
	}

	bufferBytes := buffer.Bytes()
	if p.needCompress {
		bufferBytes = common.CompressBytes(bufferBytes)
	}

	return bufferBytes
}

func (p *Packet) Compress() Interface {
	p.needCompress = true
	return p
}

func New(name common.PacketType, fields Fields) Interface {
	packet := &Packet{
		fields: fields,
		_type:  name,
	}
	return packet
}
