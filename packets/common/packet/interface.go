package packet

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"net"
)

type Fields []field.Interface

type Interface interface {
	GetType() common.PacketType

	GetFields() Fields
	GetField(fieldName field.Name) field.Interface

	Mutate(r []byte, offset int) (int, error)
	GetSize() int

	Debug(c net.Conn)

	GetCommandField() field.Interface
	SetCommandField(f field.Interface)

	Bytes() []byte
	Compress() Interface
}
