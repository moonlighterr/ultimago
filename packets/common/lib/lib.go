package lib

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/mapping/both/ping"
	"UltimaGo/packets/mapping/income/character_creation"
	"UltimaGo/packets/mapping/income/character_select"
	"UltimaGo/packets/mapping/income/game_server_login"
	"UltimaGo/packets/mapping/income/login"
	"UltimaGo/packets/mapping/income/seed"
	"UltimaGo/packets/mapping/income/select_server"
)

func FromOffset(b byte) packet.Interface {
	switch common.CommandOffset(b).PacketType() {
	case common.TypeSeed:
		return seed.New()
	case common.TypeLogin:
		return login.New()
	case common.TypeSelectServer:
		return select_server.New()
	case common.TypeGameServerLogin:
		return game_server_login.New()
	case common.TypePing:
		return ping.New(0)
	case common.TypeCharacterCreation:
		return character_creation.New()
	case common.TypeCharacterSelection:
		return character_select.New()
	}
	return nil
}
