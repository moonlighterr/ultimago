package common

import (
	"bytes"
	"encoding/binary"
)

func PadBytes(init []byte, size int) []byte {
	if len(init) == size {
		return init
	}

	toAdd := size - len(init)
	boo := make([]byte, toAdd)
	for i := 0; i < toAdd; i++ {
		boo[i] = 0x00
	}
	return boo
}

func ByteToInt(data []byte) []int {
	res := make([]int, 0)
	for _, b := range data {
		res = append(res, int(b))
	}
	return res
}

func CompressBytes(buff []byte) []byte {
	return Compress(ByteToInt(buff))
}

func Join(buffers ...[]byte) []byte {
	resultBuffer := bytes.Buffer{}
	for _, buffer := range buffers {
		resultBuffer.Write(buffer)
	}
	return resultBuffer.Bytes()
}

func GetWithPadding(buff []byte, padding int) []byte {
	return PadBytes(buff, padding)
}

func IntAsBytes(integer int, length int) []byte {
	bs := make([]byte, length)
	switch length {
	case 2:
		binary.BigEndian.PutUint16(bs, uint16(integer))
	case 4:
		binary.BigEndian.PutUint32(bs, uint32(integer))
	case 8:
		binary.BigEndian.PutUint64(bs, uint64(integer))
	}
	return bs
}
