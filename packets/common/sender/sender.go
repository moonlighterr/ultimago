package sender

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/packets/common/packet"
	"fmt"
	"net"
)

func Send(conn net.Conn, packet packet.Interface) {
	buff := packet.Bytes()
	packet.Debug(conn)
	logger.Info("Outgoing packet debug", "packets.common.sender.sender", map[string]any{
		"packet_type": packet.GetType(),
		"data":        fmt.Sprintf("% X", buff),
		"connection":  conn.RemoteAddr().String(),
	})

	conn.Write(buff)
}
