package common

type PacketType string

const (
	TypeSeed                PacketType = "seed"
	TypeLogin               PacketType = "login"
	TypeSelectServer        PacketType = "chosen_server"
	TypeServerInfo          PacketType = "server_info"
	TypeGameServerLogin     PacketType = "gameserver_login"
	TypeClientFeaturesFlags PacketType = "client_features_flags"
	TypeCharacterSelection  PacketType = "character_selection"
	TypeCharactersSelection PacketType = "characters_selection"
	TypePing                PacketType = "ping"
	TypeCharacterCreation   PacketType = "character_creation"
	TypeLightLevel          PacketType = "light_level"
	TypePersonalLightLevel  PacketType = "personal_light_level"
	TypePlayMusic           PacketType = "play_music"

	TypeMulti PacketType = "multi"
)
