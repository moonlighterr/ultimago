package reciever

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/packets/common/lib"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/server_context"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
)

func Recieve(ctx context.Context, c net.Conn, packetsChan chan packet.Interface) error {
	contextType := server_context.Type(ctx.Value(server_context.KEY).(int))
	firstPacket := true

	for {
		incomingPacketBytes := make([]byte, 1024)
		bytesReaded, err := c.Read(incomingPacketBytes)
		if err != nil {
			if err == io.EOF {
				return errors.New(fmt.Sprintf("client disconnected"))
			} else {
				return errors.New(fmt.Sprintf("cannot read from incoming connection, err: %s", err))
			}
		}

		incomingPacketBytes = incomingPacketBytes[:bytesReaded]

		//fmt.Printf(">>> %+v bytes readed %X\n", bytesReaded, incomingPacketBytes)
		logger.Info("[Debug] Reading", "packets.common.reciever.reciever", map[string]any{
			"bytes_readed":          fmt.Sprintf("%+v", bytesReaded),
			"incoming_packet_bytes": fmt.Sprintf("%X", incomingPacketBytes),
			"connection":            c.RemoteAddr().String(),
		})

		var lastOffset int

		for bytesReaded > 0 {
			if lastOffset > len(incomingPacketBytes) {
				logger.Info("Cannot get offset of incoming message", "packets.common.reciever.reciever", map[string]any{
					"incoming_packet_bytes_len": len(incomingPacketBytes),
					"last_offset":               lastOffset,
					"connection":                c.RemoteAddr().String(),
				})
				break
			}

			incomingPacket := lib.FromOffset(incomingPacketBytes[lastOffset])

			if incomingPacket == nil {
				// for first connection to game server client sends seed without leading offset
				// just 4 bytes, so need to handle this
				if contextType == server_context.GAME && firstPacket && bytesReaded > 4 {
					// skip seed 4 bytes cause of next packet will be login to game server
					// with same information
					lastOffset += 4
					bytesReaded -= 4
					firstPacket = false
					continue
				}

				fmt.Printf("Cannot handle packet with command byte % x\n", incomingPacketBytes[lastOffset:])
				logger.Info("Cannot handle packet with command byte", "packets.common.reciever.reciever", map[string]any{
					"incoming_packet_bytes_offset": fmt.Sprintf("% x", incomingPacketBytes[lastOffset:]),
					"connection":                   c.RemoteAddr().String(),
				})
				break
			}

			firstPacket = false

			lastOffset, err = incomingPacket.Mutate(incomingPacketBytes[lastOffset:incomingPacket.GetSize()], 0)

			if err != nil {
				return errors.New("There is error during packet parsing.")
			}

			incomingPacket.Debug(c)
			packetsChan <- incomingPacket

			bytesReaded -= incomingPacket.GetSize()
		}
	}
}
