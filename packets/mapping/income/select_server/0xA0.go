package select_server

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*BYTE[1] cmd
BYTE[4] seed, usually the client local ip
BYTE[4] client major version
BYTE[4] client minor version
BYTE[4] client revision version
BYTE[4] client prototype version*/

const (
	FieldChosenShard field.Name = "chosen_shard"
)

func New() packet.Interface {
	return packet.New(common.TypeSelectServer, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.Int](FieldChosenShard, 2),
	})
}
