package character_select

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
BYTE[1] cmd
BYTE[4] pattern1 (0xedededed)
BYTE[30] char name (0 terminated)
BYTE[2] unknown0
BYTE[4] clientflag (see notes)
BYTE[4] unknown1
BYTE[4] logincount
BYTE[16] unknown2
BYTE[4] slot choosen
BYTE[4] clientIP
*/

const (
	FieldUnknown1   field.Name = "unknown"
	FieldLogin      field.Name = "login"
	FieldUnknown2   field.Name = "unknown"
	FieldClientFlag field.Name = "client_flag"
	FieldUnknown3   field.Name = "unknown"
	FieldLoginCount field.Name = "login_count"
	FieldUnknown4   field.Name = "unknown"
	FieldSlotChosen field.Name = "slot"
	FieldClientIP   field.Name = "ip"
)

func New() packet.Interface {
	return packet.New(common.TypeCharacterSelection, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.Int](FieldUnknown1, 4),
		field.New[field.String](FieldLogin, 30),
		field.New[field.Int](FieldUnknown2, 2),
		field.New[field.Int](FieldClientFlag, 4),
		field.New[field.Int](FieldUnknown3, 4),
		field.New[field.Int](FieldLoginCount, 4),
		field.New[field.Int](FieldUnknown4, 16),
		field.New[field.Int](FieldSlotChosen, 4),
		field.New[field.Int](FieldClientIP, 4),
	})
}
