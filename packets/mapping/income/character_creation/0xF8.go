package character_creation

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[1] Command
	BYTE[4] pattern1 (0xedededed)
	BYTE[4] pattern2 (0xffffffff)
	BYTE[1] pattern3 (0x00)
	BYTE[30] char name
	BYTE[2] unknown0
	BYTE[4] clientflag (see notes)
	BYTE[4] unknown1
	BYTE[4] logincount
	BYTE[1] profession
	BYTE[15] unknown2
	BYTE[1] sex (see notes)
	BYTE[1] str
	BYTE[1] dex
	BYTE[1] int
	BYTE[1] skill1
	BYTE[1] skill1value
	BYTE[1] skill2
	BYTE[1] skill2value
	BYTE[1] skill3
	BYTE[1] skill3value
	BYTE[1] skill4
	BYTE[1] skill4value
	BYTE[2] skinColor
	BYTE[2] hairStyle
	BYTE[2] hairColor
	BYTE[2] facial hair
	BYTE[2] facial hair color
	BYTE[2] location # from starting list
	BYTE[2] unknown3 (Usually 0x00 in testing)
	BYTE[2] slot
	BYTE[4] clientIP
	BYTE[2] shirt color
	BYTE[2] pants color
*/

const (
	FieldPattern1        field.Name = "pattern1"
	FieldPattern2        field.Name = "pattern2"
	FieldPattern3        field.Name = "pattern3"
	FieldCharName        field.Name = "char_name"
	FieldUnknown         field.Name = "unknown"
	FieldClientFlag      field.Name = "client_flag"
	FieldLoginCount      field.Name = "login_count"
	FieldProfession      field.Name = "profession"
	FieldSex             field.Name = "sex"
	FieldStr             field.Name = "str"
	FieldDex             field.Name = "dex"
	FieldInt             field.Name = "int"
	FieldSkill1          field.Name = "skill1"
	FieldSkill1Value     field.Name = "skill1value"
	FieldSkill2          field.Name = "skill2"
	FieldSkill2Value     field.Name = "skill2value"
	FieldSkill3          field.Name = "skill3"
	FieldSkill3Value     field.Name = "skill3value"
	FieldSkill4          field.Name = "skill4"
	FieldSkill4Value     field.Name = "skill4value"
	FieldSkinColor       field.Name = "skin_color"
	FieldHairStyle       field.Name = "hair_style"
	FieldHairColor       field.Name = "hair_color"
	FieldFacialHair      field.Name = "facial_hair"
	FieldFacialHairColor field.Name = "facial_hair_color"
	FieldLocation        field.Name = "location"
	FieldSlot            field.Name = "slot"
	FieldClientIP        field.Name = "client_ip"
	FieldShirtColor      field.Name = "shirt_color"
	FieldPantsColor      field.Name = "pants_color"
)

func New() packet.Interface {
	return packet.New(common.TypeCharacterCreation, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.Int](FieldPattern1, 4),
		field.New[field.Int](FieldPattern2, 4),
		field.New[field.Int](FieldPattern3, 1),
		field.New[field.String](FieldCharName, 30),
		field.New[field.Int](FieldUnknown, 2),
		field.New[field.Int](FieldClientFlag, 4),
		field.New[field.Int](FieldUnknown, 4),
		field.New[field.Int](FieldLoginCount, 4),
		field.New[field.Int](FieldProfession, 1),
		field.New[field.Int](FieldUnknown, 15),
		field.New[field.Int](FieldSex, 1),
		field.New[field.Int](FieldStr, 1),
		field.New[field.Int](FieldDex, 1),
		field.New[field.Int](FieldInt, 1),
		field.New[field.Int](FieldSkill1, 1),
		field.New[field.Int](FieldSkill1Value, 1),
		field.New[field.Int](FieldSkill2, 1),
		field.New[field.Int](FieldSkill2Value, 1),
		field.New[field.Int](FieldSkill3, 1),
		field.New[field.Int](FieldSkill3Value, 1),
		field.New[field.Int](FieldSkill4, 1),
		field.New[field.Int](FieldSkill4Value, 1),
		field.New[field.Int](FieldSkinColor, 2),
		field.New[field.Int](FieldHairStyle, 2),
		field.New[field.Int](FieldHairColor, 2),
		field.New[field.Int](FieldFacialHair, 2),
		field.New[field.Int](FieldFacialHairColor, 2),
		field.New[field.Int](FieldLocation, 2),
		field.New[field.Int](FieldUnknown, 2),
		field.New[field.Int](FieldSlot, 2),
		field.New[field.String](FieldClientIP, 4),
		field.New[field.Int](FieldShirtColor, 2),
		field.New[field.Int](FieldPantsColor, 2),
	})
}
