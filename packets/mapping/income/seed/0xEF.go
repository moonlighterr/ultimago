package seed

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[4] seed, usually the client local ip
	BYTE[4] client major version
	BYTE[4] client minor version
	BYTE[4] client revision version
	BYTE[4] client prototype version
*/

const (
	FieldSeed                   field.Name = "seed"
	FieldClientMajorVersion     field.Name = "client_major_version"
	FieldClientMinorVersion     field.Name = "client_minor_version"
	FieldClientRevisionVersion  field.Name = "client_revision_version"
	FieldClientPrototypeVersion field.Name = "client_prototype_version"
)

func New() packet.Interface {
	return packet.New(common.TypeSeed, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.String](FieldSeed, 4),
		field.New[field.Int](FieldClientMajorVersion, 4),
		field.New[field.Int](FieldClientMinorVersion, 4),
		field.New[field.Int](FieldClientRevisionVersion, 4),
		field.New[field.Int](FieldClientPrototypeVersion, 4),
	})
}
