package game_server_login

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
BYTE[1] cmd
BYTE[4] key used
BYTE[30] sid
BYTE[30] password
*/

const (
	FieldKeyUsed  field.Name = "key_used"
	FieldLogin    field.Name = "login"
	FieldPassword field.Name = "password"
)

func New() packet.Interface {
	return packet.New(common.TypeGameServerLogin, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.String](FieldKeyUsed, 4),
		field.New[field.String](FieldLogin, 30),
		field.New[field.String](FieldPassword, 30),
	})
}
