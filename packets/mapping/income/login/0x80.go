package login

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[30] Account Name
	BYTE[30] Password
	BYTE[1] NextLoginKey value from uo.cfg on client machine.
*/

const (
	FieldLogin        field.Name = "account_name"
	FieldPassword     field.Name = "password"
	FieldNextLoginKey field.Name = "next_login_key"
)

func New() packet.Interface {
	return packet.New(common.TypeLogin, packet.Fields{
		field.New[field.Int](field.Command, 1),
		field.New[field.String](FieldLogin, 30),
		field.New[field.String](FieldPassword, 30),
		field.New[field.Int](FieldNextLoginKey, 1),
	})
}
