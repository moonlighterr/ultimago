package personal_light_level

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/serial"
)

/*
	BYTE[1] cmd
	BYTE[4] creature id
	BYTE[1] level
*/

const (
	FieldPersonalitySerial field.Name = "serial"
	FieldLevel             field.Name = "light_level"
)

func New(s serial.Interface) packet.Interface {
	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0x4E})

	serialField := field.New[field.Int](FieldPersonalitySerial, 4)
	serialField.SetData(s.Get())

	levelField := field.New[field.Int](FieldLevel, 1)
	levelField.SetData(int8(0))

	return packet.New(common.TypePersonalLightLevel, packet.Fields{
		cmdField,
		serialField,
		levelField,
	}).Compress()
}
