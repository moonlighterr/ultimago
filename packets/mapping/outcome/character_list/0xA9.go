package character_list

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/parameter"
	"UltimaGo/pkg/struct/parameter_list"
	"UltimaGo/world/player"
)

/*
	BYTE[1] cmd
	BYTE[2] packet length
	BYTE[1] number of characters (slots 5, 6 or 7)

	loop characters (5, 6 or 7):
	BYTE[30] character name
	BYTE[30] character password
	endloop(characters)

	BYTE[1] number of starting locations (cities)
	loop # of cities:
	BYTE[1] locationIndex (0-based)
	BYTE[32] city name(general name)
	BYTE[32] area of city or town
	BYTE[4] City X Coordinate
	BYTE[4] City Y Coordinate
	BYTE[4] City Z Coordinate
	BYTE[4] CIty Map ( Probably Map ID same as in mul files have to make sure )
	BYTE[4] Cliloc Description
	BYTE[4] Always 0
	endloop(# of cities)

	BYTE[4] Flags (see notes)
	BYTE[2] Last Character Slot ( for highlight )
*/

const (
	FieldCharactersArray   field.Name = "characters_array"
	FieldCharacterName     field.Name = "character_name"
	FieldCharacterPassword field.Name = "characters_password"

	FieldStartingLocationsCount        field.Name = "starting_locations_count"
	FieldStartingLocationIndex         field.Name = "starting_location_index"
	FieldStartingLocationName          field.Name = "starting_location_name"
	FieldStartingLocationArea          field.Name = "starting_location_area"
	FieldStartingLocationX             field.Name = "starting_location_x"
	FieldStartingLocationY             field.Name = "starting_location_y"
	FieldStartingLocationZ             field.Name = "starting_location_z"
	FieldStartingLocationMapId         field.Name = "starting_location_map_id"
	FieldStartingLocationDescriptionId field.Name = "starting_location_description_id"
	FieldStartingLocationNone          field.Name = "starting_location_none"

	FieldFlags              field.Name = "flags"
	FieldHighlightCharIndex field.Name = "highlight_char_index"
)

type CharFlags int

const (
	None                  CharFlags = 0x00000000
	Unk1                  CharFlags = 0x00000001
	OverwriteConfigButton CharFlags = 0x00000002
	OneCharacterSlot      CharFlags = 0x00000004
	ContextMenus          CharFlags = 0x00000008
	SlotLimit             CharFlags = 0x00000010
	AOS                   CharFlags = 0x00000020
	SixthCharacterSlot    CharFlags = 0x00000040
	SE                    CharFlags = 0x00000080
	ML                    CharFlags = 0x00000100
	Unk2                  CharFlags = 0x00000200
	UO3DClientType        CharFlags = 0x00000400
	KR                    CharFlags = 0x00000600 // uo:kr support flags
	Unk3                  CharFlags = 0x00000800
	SeventhCharacterSlot  CharFlags = 0x00001000
	Unk4                  CharFlags = 0x00002000
	NewMovementSystem     CharFlags = 0x00004000
	NewFeluccaAreas       CharFlags = 0x00008000

	ExpansionNone CharFlags = ContextMenus //
	ExpansionT2A  CharFlags = ContextMenus //
	ExpansionUOR  CharFlags = ContextMenus // None
	ExpansionUOTD CharFlags = ContextMenus //
	ExpansionLBR  CharFlags = ContextMenus //
	ExpansionAOS  CharFlags = ContextMenus | AOS
	ExpansionSE   CharFlags = ExpansionAOS | SE
	ExpansionML   CharFlags = ExpansionSE | ML
	ExpansionSA   CharFlags = ExpansionML
	ExpansionHS   CharFlags = ExpansionSA
	ExpansionTOL  CharFlags = ExpansionHS
	ExpansionEJ   CharFlags = ExpansionTOL
)

func New(pl player.Interface) packet.Interface {

	characterList := pl.GetCharacterList()

	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0xA9})

	sizeField := field.New[field.PacketSize](field.Size, 2)

	characters := []field.Interface{}
	characterArr := characterList.All()
	for i := 0; i < 7; i++ {
		var charName string
		if i < characterList.Count() {
			charName = parameter_list.Get[parameter.NAME](characterArr[i].GetParameters()).String()
		}

		charNameField := field.New[field.String](FieldCharacterName, 30)
		charNameField.SetData(charName)
		charPasswordField := field.New[field.String](FieldCharacterPassword, 30)
		charPasswordField.SetData("")
		characters = append(characters, charNameField, charPasswordField)
	}
	charactersArray := field.New[field.Slice](FieldCharactersArray, 1, characters...)
	charactersArray.SetData(int8(7))
	// characters loop

	startingLocationsCountField := field.New[field.Int](FieldStartingLocationsCount, 1)
	startingLocationsCountField.SetData(int8(1))

	// starting locations loop

	startingLocationIndexField := field.New[field.Int](FieldStartingLocationIndex, 1)
	startingLocationIndexField.SetData(int8(0))

	startingLocationNameField := field.New[field.String](FieldStartingLocationName, 32)
	startingLocationNameField.SetData("TestingName")

	startingLocationAreaField := field.New[field.String](FieldStartingLocationArea, 32)
	startingLocationAreaField.SetData("TestingArea")

	startingLocationXField := field.New[field.Int](FieldStartingLocationX, 4)
	startingLocationXField.SetData(int32(500))
	startingLocationYField := field.New[field.Int](FieldStartingLocationY, 4)
	startingLocationYField.SetData(int32(500))
	startingLocationZField := field.New[field.Int](FieldStartingLocationZ, 4)
	startingLocationZField.SetData(int32(1))

	startingLocationMapIdField := field.New[field.Int](FieldStartingLocationMapId, 4)
	startingLocationMapIdField.SetData(int32(0))

	startingLocationDescriptionIdField := field.New[field.Int](FieldStartingLocationDescriptionId, 4)
	startingLocationDescriptionIdField.SetData(int32(1))

	startingLocationNoneField := field.New[field.Int](FieldStartingLocationNone, 4)
	startingLocationNoneField.SetData(int32(0))

	startingLocationFlagsField := field.New[field.Int](FieldFlags, 4)
	startingLocationFlagsField.SetData(int32(SixthCharacterSlot | ExpansionEJ | SeventhCharacterSlot))

	startingLocationHighlightedCharIndexField := field.New[field.Int](FieldHighlightCharIndex, 2)
	startingLocationHighlightedCharIndexField.SetData(int16(-1))

	return packet.New(common.TypeCharactersSelection, packet.Fields{
		cmdField,
		sizeField,
		charactersArray,
		startingLocationsCountField,
		startingLocationIndexField,
		startingLocationNameField,
		startingLocationAreaField,
		startingLocationXField,
		startingLocationYField,
		startingLocationZField,
		startingLocationMapIdField,
		startingLocationDescriptionIdField,
		startingLocationNoneField,
		startingLocationFlagsField,
		startingLocationHighlightedCharIndexField,
	}).Compress()
}
