package play_music

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[1] cmd
	BYTE[2] musicID
*/

const (
	FieldMusicId field.Name = "music_id"
)

func New(musicID int16) packet.Interface {
	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0x6D})

	musicField := field.New[field.Int](FieldMusicId, 2)
	musicField.SetData(musicID)

	return packet.New(common.TypePlayMusic, packet.Fields{
		cmdField,
		musicField,
	}).Compress()
}
