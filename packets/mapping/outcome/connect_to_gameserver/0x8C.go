package connect_to_gameserver

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/struct/game_server"
)

/*
 0000  BYTE     cmd
 0001  UINT     gameServerIP
 0005  USHORT   gameServerPort
 0007  UINT     key              encryption key(*)
*/

const (
	FieldServerIp            field.Name = "server_ip"
	FieldServerPort          field.Name = "server_port"
	FieldServerEncryptionKey field.Name = "server_percentage"
)

func New(gameServer game_server.Interface) packet.Interface {

	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0x8C})

	ipField := field.New[field.Int](FieldServerIp, 4)
	serverIp := gameServer.GetIP()
	ipField.SetData([]byte(serverIp))

	portField := field.New[field.Int](FieldServerPort, 2)
	portField.SetData(int16(gameServer.GetPort()))

	encKeyField := field.New[field.Int](FieldServerEncryptionKey, 4)
	encKeyField.SetData([]byte{0x85, 0x92, 0xA5, 0xA8})

	return packet.New(common.TypeServerInfo, packet.Fields{
		cmdField,
		ipField,
		portField,
		encKeyField,
	})
}
