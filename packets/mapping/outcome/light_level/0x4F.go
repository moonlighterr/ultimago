package light_level

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[1] cmd
	BYTE[1] level
*/

const (
	FieldLevel field.Name = "light_level"
)

func New() packet.Interface {
	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0x4F})

	levelField := field.New[field.Int](FieldLevel, 1)
	levelField.SetData(int8(0))

	return packet.New(common.TypeLightLevel, packet.Fields{
		cmdField,
		levelField,
	}).Compress()
}
