package game_servers

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/functions"
	"UltimaGo/pkg/struct/game_server"
)

/*
 0000  BYTE     cmd
 0001  USHORT   size             The size of the packet
 0003  BYTE     flags            Server list flags
 0004  USHORT   number           The number of servers in the list
Then each server
 0000  USHORT   index            The server's 0-based index
 0002  CHAR[32] name             The name of the server
 0034  BYTE     full             The percentage of players/maximum for the server
 0035  BYTE     timezone         The server's timezone bias
 0036  UINT     address          The server's IP address in Big Endian
*/

const (
	FieldServerIp         field.Name = "server_ip"
	FieldServerTimeZone   field.Name = "server_timezone"
	FieldServerPercentage field.Name = "server_percentage"
	FieldServerIndex      field.Name = "server_index"
	FieldServerName       field.Name = "server_name"
	FieldServer           field.Name = "server"
	FieldSystemInfo       field.Name = "sys_info"
)

func New(servers []game_server.Interface) packet.Interface {
	fields := make([]field.Interface, 0)
	f := int16(0)

	for i, server := range servers {
		indexField := field.New[field.Int](FieldServerIndex, 2)
		indexField.SetData(int16(i))

		nameField := field.New[field.String](FieldServerName, 32)
		nameField.SetData(server.GetName())

		percentageField := field.New[field.Int](FieldServerPercentage, 1)
		percentageField.SetData(int8(0))

		timeZoneField := field.New[field.Int](FieldServerTimeZone, 1)
		timeZoneField.SetData(int8(3))

		ipField := field.New[field.Int](FieldServerIp, 4)

		serverIp := server.GetIP().To4()
		reversedBytes := functions.ReverseBytes(serverIp)

		ipField.SetData(reversedBytes)

		fields = append(fields, indexField, nameField, percentageField, timeZoneField, ipField)
		f++
	}

	serverField := field.New[field.Slice](FieldServer, 2, fields...)
	serverField.SetData(f)

	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0xA8})

	lenField := field.New[field.PacketSize](field.Size, 2)

	sysInfoField := field.New[field.Int](FieldSystemInfo, 1)
	sysInfoField.SetData([]byte{0x5D})

	return packet.New(common.TypeLogin, packet.Fields{
		cmdField,
		lenField,
		sysInfoField,
		serverField,
	})
}
