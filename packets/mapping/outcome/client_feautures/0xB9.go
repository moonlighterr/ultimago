package client_feautures

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
 0000  BYTE     cmd
 0001  UINT   flags
*/

const (
	FieldServerIp field.Name = "feauture_flags"
)

func New() packet.Interface {
	cmdField := field.New[field.Int](field.Command, 1)
	cmdField.SetData([]byte{0xB9})

	flagsField := field.New[field.Int](FieldServerIp, 4)
	flagsField.SetData(int32(16745183))

	return packet.New(common.TypeClientFeaturesFlags, packet.Fields{
		cmdField,
		flagsField,
	}).Compress()
}
