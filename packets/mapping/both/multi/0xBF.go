package multi

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*
	BYTE[1] Command
	BYTE[2] Length
	BYTE[2] Subcommand
	BYTE[length-5] Subcommand details
*/

const (
	FieldLength     field.Name = "length"
	FieldSubCommand field.Name = "subcommand"
	FieldSubPacket  field.Name = "subpacket"
)

func New() packet.Interface {
	commandField := field.New[field.Int](field.Command, 1)
	commandField.SetData([]byte{0xBF})

	lengthField := field.New[field.Int](FieldLength, 2)

	subcommandField := field.New[field.Slice](FieldSubPacket, 0)

	return packet.New(common.TypeMulti, packet.Fields{
		commandField,
		lengthField,
		subcommandField,
	}).Compress()
}
