package ping

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
)

/*BYTE[1] cmd
BYTE[4] seed, usually the client local ip
BYTE[4] client major version
BYTE[4] client minor version
BYTE[4] client revision version
BYTE[4] client prototype version*/

const (
	FieldSequence field.Name = "sec"
)

func New(seq int) packet.Interface {
	commandField := field.New[field.Int](field.Command, 1)
	commandField.SetData([]byte{0x73})

	seqField := field.New[field.Int](FieldSequence, 1)
	seqField.SetData(int8(seq))

	return packet.New(common.TypePing, packet.Fields{
		commandField,
		seqField,
	}).Compress()
}
