package game

import (
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/sender"
	"UltimaGo/packets/mapping/income/character_select"
	"UltimaGo/packets/mapping/outcome/light_level"
	"UltimaGo/packets/mapping/outcome/personal_light_level"
	"UltimaGo/packets/mapping/outcome/play_music"
	"UltimaGo/pkg/struct/parameter"
	"UltimaGo/pkg/struct/parameter_list"
	"UltimaGo/pkg/struct/serial"
	"UltimaGo/world/player"
)

func (r _router) CharacterSelect(pk packet.Interface, pl player.Interface) {
	loginField := field.Get[field.String](pk.GetField(character_select.FieldLogin)).Get()
	slotField := field.Get[field.Int](pk.GetField(character_select.FieldSlotChosen)).Get()

	if loginField == "" {
		pl.Disconnect()
		return
	}

	if slotField > pl.GetCharacterList().Count() {
		pl.Disconnect()
		return
	}

	for i, char := range pl.GetCharacterList().All() {
		if parameter_list.Get[parameter.NAME](char.GetParameters()).String() == loginField &&
			slotField == i {
			pl.SetCharacter(char)
		}
	}

	sender.Send(pl.GetConnection(), light_level.New())
	sender.Send(pl.GetConnection(), personal_light_level.New(serial.New(1)))
	sender.Send(pl.GetConnection(), play_music.New(24))
}
