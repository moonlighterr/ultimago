package game

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/service/router"
	"UltimaGo/pkg/struct/connection"
	"UltimaGo/world/player"
	"context"
)

type _router struct {
	pool *connection.Pool
}

func NewRouter() router.Interface {
	return &_router{pool: connection.NewPool()}
}

func (r _router) Route(ctx context.Context, pk packet.Interface, pl player.Interface) {
	pl.FlushTimeout()
	switch pk.GetType() {
	case common.TypeGameServerLogin:
		r.Login(pk, pl)
	case common.TypePing:
		r.Ping(pk, pl)
	case common.TypeCharacterCreation:
		r.CharacterCreation(pk, pl)
	case common.TypeCharacterSelection:
		r.CharacterSelect(pk, pl)
	}
}
