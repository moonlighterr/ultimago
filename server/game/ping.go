package game

import (
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/sender"
	"UltimaGo/packets/mapping/both/ping"
	"UltimaGo/world/player"
)

func (r _router) Ping(pk packet.Interface, pl player.Interface) {
	seqField := field.Get[field.Int](pk.GetField(ping.FieldSequence))
	sender.Send(pl.GetConnection(), ping.New(int(seqField)))
}
