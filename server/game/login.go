package game

import (
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/sender"
	"UltimaGo/packets/mapping/income/game_server_login"
	"UltimaGo/packets/mapping/outcome/character_list"
	"UltimaGo/packets/mapping/outcome/client_feautures"
	"UltimaGo/pkg/service/postgres/database"
	"UltimaGo/world/player"
	"fmt"
)

func (r _router) Login(pk packet.Interface, pl player.Interface) {
	loginField := field.Get[field.String](pk.GetField(game_server_login.FieldLogin)).Get()
	passwordField := field.Get[field.String](pk.GetField(game_server_login.FieldPassword)).Get()

	if loginField == "" || passwordField == "" {
		return
	}

	db := database.GetPostgre()
	var id int
	rows := db.QueryRow(
		"SELECT id FROM accounts WHERE login=$1 and password=$2",
		loginField,
		passwordField,
	)
	rows.Scan(&id)

	if id == 0 {
		// autocreate account
		err := db.QueryRow(
			"INSERT INTO accounts(login, password) VALUES($1, $2) RETURNING id",
			loginField,
			passwordField,
		).Scan(&id)

		if err != nil {
			fmt.Printf("cannot autocreate account due to err %s", err)
			return
		} else {
			fmt.Printf("autocreated account %d\n", id)
		}
	} else {
		fmt.Printf("login found %d\n", id)
	}

	pl.SetAccountId(int32(id))

	sender.Send(pl.GetConnection(), client_feautures.New())
	sender.Send(pl.GetConnection(), character_list.New(pl))
}
