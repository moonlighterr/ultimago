package game

import (
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/mapping/income/character_creation"
	"UltimaGo/pkg/struct/parameter"
	"UltimaGo/pkg/struct/parameter_list"
	"UltimaGo/world/character"
	"UltimaGo/world/player"
)

func (r _router) CharacterCreation(pk packet.Interface, pl player.Interface) {
	params := parameter_list.New()

	params.Add(parameter.New[parameter.ACCOUNT_ID](pl.GetAccountId()))
	charName := field.Get[field.String](pk.GetField(character_creation.FieldCharName)).Get()
	params.Add(parameter.New[parameter.NAME](charName))
	char := character.New(params)
	char.Save()
}
