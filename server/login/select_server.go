package login

import (
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/sender"
	"UltimaGo/packets/mapping/income/select_server"
	"UltimaGo/packets/mapping/outcome/connect_to_gameserver"
	"UltimaGo/world/player"
	"UltimaGo/world/servers"
	"fmt"
)

func (r _router) SelectServer(pk packet.Interface, pl player.Interface) {
	connectionInPool := r.pool.FindByConnection(pl.GetConnection())

	if connectionInPool == nil {
		fmt.Printf("cannot hanle login due of lack seed packet before\n")
		pl.GetConnection().Close()
	}

	serverIndexField := field.Get[field.Int](pk.GetField(select_server.FieldChosenShard))

	serverInList := servers.Index(int(serverIndexField))

	if serverInList != nil {
		sender.Send(pl.GetConnection(), connect_to_gameserver.New(serverInList))
	}
}
