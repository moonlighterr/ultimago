package login

import (
	"UltimaGo/packets/common"
	"UltimaGo/packets/common/packet"
	"UltimaGo/pkg/service/router"
	"UltimaGo/pkg/struct/connection"
	"UltimaGo/world/player"
	"context"
)

type _router struct {
	pool *connection.Pool
}

func NewRouter() router.Interface {
	return &_router{pool: connection.NewPool()}
}

func (r _router) Route(ctx context.Context, pk packet.Interface, pl player.Interface) {
	pl.FlushTimeout()
	switch pk.GetType() {
	case common.TypeSeed:
		r.Seed(pk, pl)
	case common.TypeLogin:
		if !r.LogOn(pk, pl) {
			pl.GetConnection().Close()
		}
	case common.TypeSelectServer:
		r.SelectServer(pk, pl)
	}
}
