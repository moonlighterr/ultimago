package login

import (
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/mapping/income/seed"
	"UltimaGo/pkg/struct/connection"
	"UltimaGo/world/player"
	"fmt"
)

func (r _router) Seed(pk packet.Interface, pl player.Interface) {
	seedField := pk.GetField(seed.FieldSeed)
	if seedField == nil {
		fmt.Printf("there is no seed field in packet\n")
		return
	}

	seedArray := [4]byte{}
	for i, e := range seedField.Bytes() {
		seedArray[i] = e
	}
	r.pool.Add(&connection.Structure{
		Seed:       seedArray,
		Connection: pl.GetConnection(),
	})
}
