package login

import (
	"UltimaGo/internal/service/logger"
	"UltimaGo/packets/common/field"
	"UltimaGo/packets/common/packet"
	"UltimaGo/packets/common/sender"
	"UltimaGo/packets/mapping/income/login"
	"UltimaGo/packets/mapping/outcome/game_servers"
	"UltimaGo/pkg/service/postgres/database"
	"UltimaGo/world/player"
	"UltimaGo/world/servers"
)

func (r _router) LogOn(pk packet.Interface, pl player.Interface) bool {
	loginField := field.Get[field.String](pk.GetField(login.FieldLogin)).Get()
	passwordField := field.Get[field.String](pk.GetField(login.FieldPassword)).Get()

	if loginField == "" || passwordField == "" {
		return false
	}

	db := database.GetPostgre()
	var id int
	rows := db.QueryRow(
		"SELECT id FROM accounts WHERE login=$1 and password=$2",
		loginField,
		passwordField,
	)
	rows.Scan(&id)

	if id == 0 {
		// autocreate account
		err := db.QueryRow(
			"INSERT INTO accounts(login, password) VALUES($1, $2) RETURNING id",
			loginField,
			passwordField,
		).Scan(&id)

		if err != nil {
			logger.Warn("Autocreate account error", "server.login.logon", map[string]any{
				"error":      err.Error(),
				"connection": pl.GetConnection().RemoteAddr().String(),
			})
			return false
		} else {
			logger.Info("Autocreate account", "server.login.logon", map[string]any{
				"account_id": id,
				"connection": pl.GetConnection().RemoteAddr().String(),
			})
		}
	} else {
		logger.Info("Login found", "server.login.logon", map[string]any{
			"account_id": id,
			"connection": pl.GetConnection().RemoteAddr().String(),
		})
	}

	pl.SetAccountId(int32(id))

	serverList := servers.All()
	sender.Send(pl.GetConnection(), game_servers.New(serverList))
	return true
}
