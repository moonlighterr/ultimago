package logger

import (
	"UltimaGo/pkg/service/logger"
	"UltimaGo/pkg/struct/server_context"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"time"
)

type localLogger struct {
	*zap.SugaredLogger
	mongo      *mongo.Client
	collection *mongo.Collection
	ctx        context.Context
}

func (l *localLogger) ApplyMongo(ctx context.Context, mc *mongo.Client) {
	l.ctx = ctx
	l.mongo = mc
	l.collection = mc.Database("uo_logs").Collection("uo_logs")
	//l.collection.DeleteMany(ctx, bson.D{})
	Info("Applied mongoDB logs writer", "service.logger.logger")
}

var localLoggerObj *localLogger

func Logger(ctx context.Context) logger.Interface {
	localLoggerObj.ctx = ctx
	return localLoggerObj
}

func init() {
	l, _ := zap.NewProduction(zap.AddCallerSkip(1))
	localLoggerObj = &localLogger{
		SugaredLogger: l.Sugar(),
		mongo:         nil,
	}
}

func Warn(message, category string, data ...map[string]any) {
	localLoggerObj.Warnw(message, getLogs("warn", category, data...)...)
	log("warn", message, category, data...)
}

func Info(message, category string, data ...map[string]any) {
	localLoggerObj.Infow(message, getLogs("info", category, data...)...)
	log("info", message, category, data...)
}

func log(level string, message, category string, data ...map[string]any) {
	if localLoggerObj.collection != nil {
		_, err := localLoggerObj.collection.InsertOne(localLoggerObj.ctx, getMongoLog(level, message, category, data...))
		if err != nil {
			//Warn("Cannot write to applied mongoDB logs", "logger", map[string]any{"error": err})
			if err == context.Canceled {
				_ = localLoggerObj.Sync()
			}
		}
		//id := res.InsertedID
	}
}

func getMongoLog(level string, message, category string, data ...map[string]any) bson.D {
	var mongoBsonD bson.D
	for _, dataObj := range data {
		for fieldName, field := range dataObj {
			mongoBsonD = append(mongoBsonD, bson.E{fieldName, field})
		}
	}
	mongoBsonD = append(mongoBsonD, bson.E{"message", message}, bson.E{"category", category})
	if server := localLoggerObj.ctx.Value("server_context"); server != "" && server != nil {
		mongoBsonD = append(mongoBsonD, bson.E{"server", server_context.GetContextServerType(server.(int))})
	}
	mongoBsonD = append(mongoBsonD, bson.E{
		"ts",
		float64(time.Now().UnixMicro()) * 0.000001,
	})
	mongoBsonD = append(mongoBsonD, bson.E{
		"level",
		level,
	})
	return mongoBsonD
}

func getLogs(level string, category string, data ...map[string]any) []any {
	var tmp []any
	if len(data) > 0 {
		for _, t := range data {
			for k, v := range t {
				tmp = append(tmp, k)
				tmp = append(tmp, v)
			}
		}
	}
	tmp = append(tmp, "category")
	tmp = append(tmp, category)
	tmp = append(tmp, "level")
	tmp = append(tmp, level)

	if server := localLoggerObj.ctx.Value("server_context"); server != "" && server != nil {
		tmp = append(tmp, "server")
		tmp = append(tmp, server_context.GetContextServerType(server.(int)))
	}

	return tmp
}
