package config

import (
	config_service "UltimaGo/pkg/service/config"
	localViper "UltimaGo/pkg/service/config/config/viper"
	"github.com/spf13/viper"
)

const LOGIN_SERVER_PORT int = 2593
const LOGIN_HTTP_PORT int = 8082
const SERVER_KEY = "password"

type Config struct {
	LoginServerIp       string `mapstructure:"login_server_ip"`
	LoginServerPort     int    `mapstructure:"login_server_port"`
	LoginServerHttpPort int    `mapstructure:"login_server_http_port"`
	LoginServerKey      string `mapstructure:"login_server_key"`
}

var config *Config

func GetConfig(filename ...string) Config {
	if config != nil {
		return *config
	}

	config = &Config{}
	viper.SetDefault("login_server_ip", "")
	viper.SetDefault("login_server_port", 0)
	viper.SetDefault("login_server_http_port", 0)
	viper.SetDefault("login_server_key", "")
	config_service.LoadConfig(config, filename[0], localViper.ParseFile)
	return *config
}
