package main

import (
	"UltimaGo/cmd/login_server/config"
	"UltimaGo/internal/service/logger"
	"UltimaGo/pkg/service/easy_handle"
	"UltimaGo/pkg/service/mongo/database"
	httpServer "UltimaGo/pkg/service/net/http"
	"UltimaGo/pkg/struct/game_server"
	"UltimaGo/server/login"
	"UltimaGo/world/world"
	"os/signal"
	"sync"

	"UltimaGo/pkg/service/net/connecter"
	"UltimaGo/pkg/service/net/listener"
	"UltimaGo/pkg/struct/server_context"
	"UltimaGo/world/servers"
	"context"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"strconv"
)

// 1. load all existing users
// 2. autocreate user if it does not exist
// 3. fill temporal table with seed
// 4. flush seed table on startup

const CONFIG_FILE = "login_config"

func main() {
	// first of all create interface which carry all necessary staff like
	// context - for handling closing
	// waitgroup - main process sync
	// signal channel - to send interrupts
	ctx, cancelFn := context.WithCancel(context.Background())
	ctx = server_context.GetServerTypeContext(ctx, server_context.LOGIN)
	easy := easy_handle.New( // main handler
		ctx,
		&sync.WaitGroup{},
		make(chan os.Signal),
	)
	signal.Notify(easy.GetDoneChan(), os.Interrupt, os.Kill)

	// create connection to mongoDB (elastic) to write logs
	// and set it to logger interface
	// disable writing by commenting that method call
	// logger service must be side context to prevent closing while we got signal, close it in the end
	// when all another services writes their logs
	loggerContext, loggerContextCancelFn := context.WithCancel(
		// we need standalone context with our server type
		server_context.GetServerTypeContext(context.Background(), server_context.LOGIN),
	)
	logger.Logger(loggerContext).ApplyMongo(
		loggerContext,
		database.GetMongo(loggerContext, easy.GetDoneChan()),
	)

	logger.Info("Starting application", "main")

	// static function with preloaded/default config values
	config.GetConfig(CONFIG_FILE)

	// serving specific routes to interact with gameserver
	httpServer.ServeRoutes(easy, config.GetConfig().LoginServerHttpPort, map[string]func(http.ResponseWriter, *http.Request){
		"/alive":                HandleAlive,
		"/game_server/register": HandleRegisterServer,
	})

	uoWorld := world.New()
	routing := login.NewRouter()

	// start socket listener
	connectionsChan := make(chan net.Conn)
	defer close(connectionsChan)
	// start socket listener, pass connection to channel
	listener.New(easy, config.GetConfig().LoginServerPort, connectionsChan)
	// establish reading from connections
	connecter.New(easy, connectionsChan, uoWorld, routing.Route)

	select {
	case <-easy.GetDoneChan():
		logger.Warn("Got signal to stop application", "main")
		cancelFn()
	}

	easy.GetWaitGroup().Wait()
	logger.Warn("Server gracefully stopped", "main")
	loggerContextCancelFn()
}

func HandleAlive(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
}

func HandleRegisterServer(res http.ResponseWriter, req *http.Request) {

	if err := req.ParseForm(); err != nil {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	if !req.Form.Has("key") {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	key := req.Form.Get("key")
	if key != config.GetConfig().LoginServerKey {
		res.WriteHeader(http.StatusForbidden)
		return
	}

	serverName := req.Form.Get("name")
	serverIp := req.Form.Get("ip")
	serverPort, _ := strconv.Atoi(req.Form.Get("port"))

	servers.Add(game_server.New(serverName, &net.TCPAddr{
		IP:   net.ParseIP(serverIp),
		Port: serverPort,
	}))

	logger.Info("Successfully registered new game server", "main", map[string]any{
		"server_name": serverName,
		"server_ip":   serverIp,
		"server_port": serverPort,
	})

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("OK"))
}
