package config

import (
	config_service "UltimaGo/pkg/service/config"
	localViper "UltimaGo/pkg/service/config/config/viper"
	"UltimaGo/pkg/service/login_alive/handle_type"
	"github.com/spf13/viper"
	"net/url"
	"strconv"
)

type Config struct {
	GameServerIp         string                `mapstructure:"game_server_ip"`
	GameServerPort       int                   `mapstructure:"game_server_port"`
	GameServerHandleType handle_type.HanleType `mapstructure:"game_server_handle_type"`

	LoginServerIp                     string `mapstructure:"login_server_ip"`
	LoginServerPort                   int    `mapstructure:"login_server_port"`
	LoginServerRegisterGameServerPath string `mapstructure:"login_server_register_game_server_path"`
	LoginServerAlivePath              string `mapstructure:"login_server_alive_path"`
	LoginServerKey                    string `mapstructure:"login_server_key"`
}

func (c Config) GetLoginServerAliveUrl() url.URL {
	return url.URL{
		Scheme: "http",
		Host:   c.LoginServerIp + ":" + strconv.Itoa(c.LoginServerPort),
		Path:   c.LoginServerAlivePath,
	}
}

var config *Config

func GetConfig(filename ...string) Config {
	if config != nil {
		return *config
	}

	config = &Config{}
	viper.SetDefault("game_server_ip", "")
	viper.SetDefault("game_server_port", 0)
	viper.SetDefault("game_server_handle_type", handle_type.NEVER)
	viper.SetDefault("login_server_ip", "")
	viper.SetDefault("login_server_port", 0)
	viper.SetDefault("login_server_register_game_server_path", "")
	viper.SetDefault("login_server_alive_path", "")
	viper.SetDefault("login_server_key", "")
	config_service.LoadConfig(config, filename[0], localViper.ParseFile)
	return *config
}
