package main

import (
	"UltimaGo/cmd/game_server/config"
	"UltimaGo/internal/service/logger"
	"UltimaGo/pkg/service/easy_handle"
	"UltimaGo/pkg/service/login_alive/alive_handler"
	"UltimaGo/pkg/service/login_alive/handle_type"
	"UltimaGo/pkg/service/login_alive/status"
	"UltimaGo/pkg/service/mongo/database"
	"UltimaGo/pkg/service/net/connecter"
	"UltimaGo/pkg/service/net/listener"
	"UltimaGo/pkg/struct/server_context"
	"UltimaGo/server/game"
	"UltimaGo/world/world"
	"context"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"time"
)

// CONFIG_FILE register our game server though sending packet
const CONFIG_FILE = "config"
const SERVER_NAME = "UltimaGo"

func main() {
	// first of all create interface which carry all necessary staff like
	// context - for handling interruption
	// waitgroup - service waiting for shutdown
	// signal channel - to send interrupts
	ctx, cancelFn := context.WithCancel(context.Background())
	ctx = server_context.GetServerTypeContext(ctx, server_context.GAME)
	easy := easy_handle.New( // main handler
		ctx,
		&sync.WaitGroup{},
		make(chan os.Signal),
	)
	signal.Notify(easy.GetDoneChan(), os.Interrupt, os.Kill)

	// create connection to mongoDB (elastic) to write logs
	// and set it to logger interface
	// disable writing by commenting that method call
	// logger service must be side context to prevent closing while we got signal, close it in the end
	loggerContext, loggerContextCancelFn := context.WithCancel(
		// we need standalone context with our server type
		server_context.GetServerTypeContext(context.Background(), server_context.GAME),
	)
	logger.Logger(loggerContext).ApplyMongo(
		loggerContext,
		database.GetMongo(loggerContext, easy.GetDoneChan()),
	)

	logger.Info("Starting application", "main")

	_ = config.GetConfig(CONFIG_FILE)

	// single service for checking alive status for login server
	aH := alive_handler.New(easy, config.GetConfig().GetLoginServerAliveUrl(), handle_type.FOREVER)
	go func() {
		ticker := time.NewTicker(time.Second * 10)
		for range ticker.C {
			if aH.GetStatus() == status.ALIVE {
				RegisterServer(easy.GetDoneChan())
				return
			}
		}
	}()

	uoWorld := world.New()
	routing := game.NewRouter()

	// connections chan filled with listener and process by connector
	connectionsChan := make(chan net.Conn)
	defer close(connectionsChan)

	// start socket listener, pass connection to channel
	listener.New(easy, config.GetConfig().GameServerPort, connectionsChan)
	// establish reading from connections
	connecter.New(easy, connectionsChan, uoWorld, routing.Route)

	select {
	case <-easy.GetDoneChan():
		logger.Warn("Got signal to stop application", "main")
		cancelFn()
	}

	easy.GetWaitGroup().Wait()
	logger.Warn("Server gracefully stopped", "main")
	loggerContextCancelFn()
	os.Exit(0)
}

func RegisterServer(doneChan chan os.Signal) {
	urlData := url.Values{
		"name": {SERVER_NAME},
		"ip":   {config.GetConfig().GameServerIp},
		"port": {strconv.Itoa(config.GetConfig().GameServerPort)},
		"key":  {config.GetConfig().LoginServerKey},
	}

	loginServerUrl := "http://" + config.GetConfig().LoginServerIp +
		":" + strconv.Itoa(config.GetConfig().LoginServerPort) +
		config.GetConfig().LoginServerRegisterGameServerPath

	resp, err := http.PostForm(
		loginServerUrl,
		urlData,
	)
	if err != nil {
		logger.Warn("Registering server ends with error", "main", map[string]any{
			"error":       err.Error(),
			"url":         loginServerUrl,
			"server_name": SERVER_NAME,
		})
		doneChan <- os.Interrupt
	}

	if resp.StatusCode == http.StatusOK {
		logger.Warn("Successfully registered new game server", "main", map[string]any{
			"url":         loginServerUrl,
			"server_name": SERVER_NAME,
		})
	}
}
